/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import javax.swing.table.DefaultTableModel;

/**
 *
 * @author MK
 */



public class ClientsTableModel extends DefaultTableModel {
    
    /**
	 * 
	 */
	private static final long serialVersionUID = -1445678461909320629L;
	public final int KEY_INDEX = 2; // Client ID column
    
    public ClientsTableModel(Object rowData[][], Object columnNames[]) {
        super(rowData, columnNames);
    }
    
    public ClientsTableModel() {
        super();
    }

    @Override
    public boolean isCellEditable(int row, int col) {
        return false;
    }
    
    @Override
    public void addRow(Object[] rowData) {
    	updateCell(rowData);
    }
    
    @SuppressWarnings("unchecked")
	public synchronized void updateCell(Object[] _data) {        

        boolean isNew = true;
        
        if (_data == null) {
            return;
        }
        
        for (int i = 0 ; i < dataVector.size() && isNew; i++) {	
            if (_data[KEY_INDEX].equals(getValueAt(i, KEY_INDEX))) {
                isNew = false;
                dataVector.setElementAt(convertToVector(_data), i);
            }
        }
        
        if (isNew == true) {
        	super.addRow(_data);            
        }
        
        newDataAvailable(null);
    }     
    
}