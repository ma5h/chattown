package server.Network;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class CT_Socket {

	private Socket _socket;
	private ObjectInputStream  _inputstream;
	private ObjectOutputStream _outputStream;
	
	public CT_Socket(Socket socket, ObjectInputStream  inputstream, ObjectOutputStream outputStream) {
		_socket = socket;
		_inputstream = inputstream;
		_outputStream = outputStream;
	}

	public Socket get_socket() {
		return _socket;
	}

	public ObjectInputStream get_inputstream() {
		return _inputstream;
	}

	public ObjectOutputStream get_outputStream() {
		return _outputStream;
	}
	
}
