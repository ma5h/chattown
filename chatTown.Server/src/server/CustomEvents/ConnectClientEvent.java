package server.CustomEvents;

import java.util.EventObject;

import chatown.Client;

public class ConnectClientEvent extends EventObject {

	private static final long serialVersionUID = 1300142366218586394L;
	
	private Client _client;

	public ConnectClientEvent(Object source, Client client) {
	     super(source);
	     _client = client;
	 }

	public Client get_client() {
		return _client;
	}

}
