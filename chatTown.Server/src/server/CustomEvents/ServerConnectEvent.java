package server.CustomEvents;

import java.util.EventObject;

public class ServerConnectEvent extends EventObject {

	private static final long serialVersionUID = 8428538083257051391L;
	
	private int _port;

	public ServerConnectEvent(Object source, int port) {
	     super(source);
	     _port = port;
	 }

	public int get_port() {
		return _port;
	}

}