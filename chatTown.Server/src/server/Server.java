package server;

import javax.swing.JFrame;

import CustomEvents.ICustomEventListener;
import CustomEvents.OpenMsgClientEvent;

import server.CustomEvents.ConnectClientEvent;
import server.CustomEvents.ServerConnectEvent;
import server.CustomEvents.ServerDisconnectEvent;
import server.Network.CT_ServerSocket;

import java.util.EventObject;

/**
 *
 * @author MK
 */
public class Server implements ICustomEventListener {

    /**
     * @param args the command line arguments
     */
	
	private static ServerFrame _frame = null;
	private CT_ServerSocket _serverSocket = null;
	private Thread _networkThread;
	
    public static void main(String[] args) {
        
        java.awt.EventQueue.invokeLater(new Runnable() {
        	public void run() {
        		final String orgName = Thread.currentThread().getName();
                Thread.currentThread().setName(orgName + " - ServerFrame Thread");
        		
                _frame = new ServerFrame();
                _frame.setTitle("chatTown SERVER");
                _frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                _frame.pack();
                _frame.setVisible(true); 
                
                new Server();
        	}
        });
    }
    
    public Server() {
    	
    	_frame.registerConnectServerEvent(this);
    	_frame.registerDisconnectServerEvent(this);
    	
    	
    	_serverSocket = new CT_ServerSocket(); 
    	_serverSocket.registerConnectClientEvent(this);  
    	_serverSocket.registerOpenMsgClientEvent(this);
    	_networkThread = new Thread(_serverSocket);
    	_networkThread.start();   	
    }

	@Override
	public void handleEvent(final EventObject e) {
		if (_frame == null) {return;}
		
		if (e instanceof ConnectClientEvent) {
						
	        java.awt.EventQueue.invokeLater(new Runnable() {
	        	public void run() {
	        		_frame.newClientConnected(((ConnectClientEvent)e).get_client());
	        	}
	        });			
		}
		else if (e instanceof ServerConnectEvent) {
			_serverSocket.connect(((ServerConnectEvent)e).get_port());
		}
		else if (e instanceof ServerDisconnectEvent) {
			_serverSocket.disconnect();
		}
		else if (e instanceof OpenMsgClientEvent) {
	        java.awt.EventQueue.invokeLater(new Runnable() {
	        	public void run() {
	        		_frame.handleOpenMsg(((OpenMsgClientEvent)e).get_msg().get_client());
	        	}
	        });
		}
	}

}
