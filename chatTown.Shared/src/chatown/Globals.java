package chatown;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.HashMap;

public final class Globals {
	
	private static HashMap<String, String> _setting = new HashMap<String, String>();

	public static int DEFAULT_SERVER_PORT = 8050;
	public static int DEFAULT_CLIENT_PORT = 8060;
	public static String DEFAULT_SERVER_IP = "127.0.0.1";
	//public static String DEFAULT_SERVER_IP = "132.68.204.183";
	
	
	// client evens constants
	public static int CONNECT = 0;
	public static int DISCONNECT = 1;
	public static int OPEN_MSG = 2;
	
	// server evens constants
	public static int SERVER_CONNECT = 0;
	public static int SERVER_DISCONNECT = 1;
	
	
	public static void init() {
		try {

			// Open the file that is the first 
			// command line parameter
			FileInputStream fstream;
			try {
				fstream = new FileInputStream("settings.txt");
			} catch (FileNotFoundException e1) {
				return;
			}

			// Get the object of DataInputStream
			DataInputStream in = new DataInputStream(fstream);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strLine;
			
			String[] strArr;
			//Read File Line By Line
			while ((strLine = br.readLine()) != null) {
				if (strLine.startsWith("//")) {
					continue;
				}
				strArr = strLine.split("=");
				if (strArr.length == 2) {
					_setting.put(strArr[0], strArr[1]);
				}
			}
			
			// Close the input stream
			in.close();
		} catch (Exception e) { //Catch exception if any
			System.err.println("Error in Globals init (1): " + e.getMessage());
		}
		
		try {
		if (_setting.containsKey("server ip")) {
			DEFAULT_SERVER_IP = _setting.get("server ip");
		}
		else if (_setting.containsKey("server port")) {
			DEFAULT_SERVER_PORT = Integer.parseInt(_setting.get("server port"));
		}
		else if (_setting.containsKey("client port")) {
			DEFAULT_CLIENT_PORT = Integer.parseInt(_setting.get("client port"));
		}
		} catch (NumberFormatException e1) {
			System.err.println("Error in Globals init (2): " + e1.getMessage());
		}
	}
	
}

