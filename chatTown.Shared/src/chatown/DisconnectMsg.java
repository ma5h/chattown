package chatown;

import java.io.Serializable;

public class DisconnectMsg implements Serializable {

	private static final long serialVersionUID = -3373470933737105754L;
	private Client _client;
	
	public DisconnectMsg(Client client) {
		_client = client;
	}

	public Client get_client() {
		return _client;
	}

}
