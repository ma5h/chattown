package chatown;

import java.io.Serializable;

public class OpenMsg implements Serializable{

	private static final long serialVersionUID = 783437897406670341L;
	
	private String _msg;
	private Client _client;
	
	public OpenMsg(Client client, String msg) {
		_msg = msg;
		_client = client;
	}

	public String get_msg() {
		return _msg;
	}

	public Client get_client() {
		return _client;
	}
	
}
