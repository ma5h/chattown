package chatown;

import java.io.Serializable;

public class ClientIdentityMsg implements Serializable{
	

	private static final long serialVersionUID = -6341933726501985210L;
	
	private String _ID = null;
	private String _nickname = null;
	
	public ClientIdentityMsg(String nickname) {
		_nickname = nickname;
	}
	
	public ClientIdentityMsg(String nickname, String ID) {
		_nickname = nickname;
		_ID = ID;
	}

	public String get_ID() {
		return _ID;
	}

	public String get_nickname() {
		return _nickname;
	}
}
