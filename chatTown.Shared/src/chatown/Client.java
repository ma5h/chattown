package chatown;

import java.io.Serializable;

public class Client implements Serializable{
	
	private static final long serialVersionUID = 4517863195607407177L;
	
	private String _IP;
	private int _numOpenMsgs = 0;
	private String _nickName;
	private String _ID;
	
	public Client(String IP, String nickName, String ID) {
		_IP = IP;
		_nickName = nickName;
		_ID = ID;
	}
	
	public void inc_numOpenMsgs() {
		_numOpenMsgs++;
	}

	public String get_IP() {
		return _IP;
	}

	public int get_numOpenMsgs() {
		return _numOpenMsgs;
	}

	public String get_nickName() {
		return _nickName;
	}

	public String get_ID() {
		return _ID;
	}
	
	public String toString() {
		return get_nickName() + " (" + get_ID() + ")" ;
	}
}
