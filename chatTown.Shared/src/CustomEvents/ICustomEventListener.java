package CustomEvents;

import java.util.EventObject;

public interface ICustomEventListener {
	public void handleEvent(EventObject e);
}
