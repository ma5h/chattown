package CustomEvents;

import java.util.ArrayList;
import java.util.EventObject;
import java.util.Iterator;

public class CustomEventSource {
	
	private ArrayList<ICustomEventListener> _listeners = new ArrayList<ICustomEventListener>();


	public synchronized void addEventListener(ICustomEventListener listener)	{
		_listeners.add(listener);
	}

	public synchronized void removeEventListener(ICustomEventListener listener)	{
		_listeners.remove(listener);
	}
	
	// call this method whenever you want to notify
	// the event listeners of the particular event
	public synchronized void fireEvent(EventObject event) {
		Iterator<ICustomEventListener> i = _listeners.iterator();
		while (i.hasNext()) {
			((ICustomEventListener) i.next()).handleEvent(event);
		}
	}
}
