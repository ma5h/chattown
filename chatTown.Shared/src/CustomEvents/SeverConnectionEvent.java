package CustomEvents;

import java.util.EventObject;

public class SeverConnectionEvent extends EventObject {

	private static final long serialVersionUID = 3726955849095249611L;
	
	private boolean _isServerConnected;

	public SeverConnectionEvent(Object source, boolean isServerConnected) {
	     super(source);
	     _isServerConnected = isServerConnected;
	 }

	public boolean isServerConnected() {
		return _isServerConnected;
	}


}
