package CustomEvents;

import java.util.EventObject;

import chatown.OpenMsg;

public class OpenMsgClientEvent extends EventObject {

	private static final long serialVersionUID = -3088774296219010620L;
	
	private OpenMsg _msg;

	public OpenMsgClientEvent(Object source, OpenMsg msg) {
	     super(source);
	     _msg = msg;
	 }

	public OpenMsg get_msg() {
		return _msg;
	}
}

