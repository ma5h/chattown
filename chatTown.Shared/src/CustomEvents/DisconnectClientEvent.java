package CustomEvents;

import java.util.EventObject;

import chatown.Client;

public class DisconnectClientEvent extends EventObject {

	private static final long serialVersionUID = 9044482035911859440L;
	
	private Client _client;

	public DisconnectClientEvent(Object source, Client client) {
	     super(source);
	     _client = client;
	 }

	public Client get_client() {
		return _client;
	}

}
