package client;

import javax.swing.*;
import javax.swing.text.*;
import java.awt.BorderLayout;

public class RoomPanel extends JPanel {

	private static final long serialVersionUID = 6426406563841330998L;
	
	private JTextPane tpMsgsPanel;	
	private StyledDocument docMsgsPanel;
	private static String newline = "\n";
	private String _clientID;

	public RoomPanel(String clientID, boolean isOpenRoom) {
		_clientID = clientID;
		setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		add(scrollPane, BorderLayout.CENTER);
		
		tpMsgsPanel = new JTextPane();
		scrollPane.setViewportView(tpMsgsPanel);
		tpMsgsPanel.setEditable(false);
		docMsgsPanel = tpMsgsPanel.getStyledDocument();
		addStylesToDocument(docMsgsPanel);
	}
	
    protected void addStylesToDocument(StyledDocument doc) {
        //Initialize some styles.
        Style def = StyleContext.getDefaultStyleContext().getStyle(StyleContext.DEFAULT_STYLE);

        Style regular = doc.addStyle("regular", def);
        StyleConstants.setFontFamily(def, "SansSerif");

        Style s = doc.addStyle("italic", regular);
        StyleConstants.setItalic(s, true);

        s = doc.addStyle("bold", regular);
        StyleConstants.setBold(s, true);

        s = doc.addStyle("small", regular);
        StyleConstants.setFontSize(s, 10);

        s = doc.addStyle("large", regular);
        StyleConstants.setFontSize(s, 16);
    }
    
    public void addMsg(String clientName, String msg) {
		
		try {
			docMsgsPanel.insertString(docMsgsPanel.getLength(), clientName + ": ", docMsgsPanel.getStyle("bold"));
			docMsgsPanel.insertString(docMsgsPanel.getLength(), msg + newline, docMsgsPanel.getStyle("regular"));
		
		} catch (BadLocationException e) { e.printStackTrace(); }		
	}

	public String get_clientID() {
		return _clientID;
	}

}
