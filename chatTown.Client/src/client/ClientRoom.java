package client;

import javax.swing.JPanel;

import chatown.Client;

public class ClientRoom {

	private Client _client;
	private JPanel _panel;
	private TabHeader _header;
	
	public ClientRoom(Client client, JPanel panel, TabHeader header) {
		_client = client;
		_panel = panel;
		_header = header;
		
	}
	
	public Client get_client() {
		return _client;
	}

	public JPanel get_panel() {
		return _panel;
	}

	public TabHeader get_header() {
		return _header;
	}
	
}
