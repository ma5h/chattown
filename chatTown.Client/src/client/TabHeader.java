package client;

import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.FlowLayout;
import java.awt.Color;

public class TabHeader extends JPanel {

	private static final long serialVersionUID = 919873072807303033L;

	public JLabel lblIcon;
	
	public TabHeader(String name) {
		setBackground(new Color(244, 164, 96));
		setOpaque(false);
		JLabel lblName = new JLabel(name);
		lblIcon = new JLabel("");
				
		Image imageSend = getToolkit().getImage(getClass().getResource("/resources/close.png")); 
		Image scaledImageSend = imageSend.getScaledInstance(16, 16, Image.SCALE_DEFAULT);	
		lblIcon.setIcon(new ImageIcon(scaledImageSend));
		FlowLayout flowLayout = new FlowLayout(FlowLayout.CENTER, 0, 0);
		setLayout(flowLayout);
		add(lblName);
		
		JLabel lblSep = new JLabel("  ");
		add(lblSep);
		add(lblIcon);
	}
}
