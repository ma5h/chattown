package client.CustomEvents;

import java.util.EventObject;

public class MsgReceivedEvent extends EventObject {

	private static final long serialVersionUID = 7885673320100785899L;
	private String _msg;
	private String _senderID;
	
	public MsgReceivedEvent(Object source, String msg, String senderID) {
		super(source);
		_msg = msg;
		_senderID = senderID;
	}

	public String get_msg() {
		return _msg;
	}

	public String get_senderID() {
		return _senderID;
	}

}
