package client.CustomEvents;

import java.util.EventObject;

public class ClientFromClientDisconnect extends EventObject {

	private static final long serialVersionUID = 2227695348729950457L;
	private String _clientID; 
	
	public ClientFromClientDisconnect(Object source, String clientID) {
		super(source);
		_clientID = clientID;
	}

	public String get_clientID() {
		return _clientID;
	}

}
