package client.CustomEvents;

import java.util.EventObject;

public class SendMsgToClientEvent extends EventObject {

	private static final long serialVersionUID = -2814952189839973156L;
	
	private String _msg;
	private String _clientID;

	public SendMsgToClientEvent(Object source, String clientID, String msg) {
		super(source);
		_msg = msg;
		_clientID = clientID;
	}

	public String get_msg() {
		return _msg;
	}

	public String get_clientID() {
		return _clientID;
	}

}