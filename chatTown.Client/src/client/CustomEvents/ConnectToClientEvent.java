package client.CustomEvents;

import java.util.EventObject;

import chatown.Client;

public class ConnectToClientEvent extends EventObject {

	private static final long serialVersionUID = 5734383774444060761L;
	
	private Client _client;

	public ConnectToClientEvent(Object source, Client client) {
		super(source);
		_client = client;
	}

	public Client get_client() {
		return _client;
	}

}
