package client.CustomEvents;

import java.util.EventObject;

import chatown.Client;

public class NewClientsListEvent  extends EventObject {

	private static final long serialVersionUID = -4854350334570247094L;
	
	private Client[] _clientsMap;
	private String _myClientID;

	public NewClientsListEvent(Object source, Client[] clientsMap, String myClientID) {
	     super(source);
	     _clientsMap = clientsMap;
	     _myClientID = myClientID;
	 }

	public Client[] get_clientsMap() {
		return _clientsMap;
	}

	public String get_myClientID() {
		return _myClientID;
	}
}
