package client.CustomEvents;

import java.util.EventObject;

public class SettingsFilledEvent extends EventObject {

	private static final long serialVersionUID = -1376994351087601895L;
	
	private String _nickname;
	private String _serverIP;
	private String _serverPort;
	private String _clientsPort;

	public SettingsFilledEvent(Object source, String nickname, String serverIP, String serverPort, String clientsPort) {
	     super(source);
	     _nickname = nickname;
	     _serverIP = serverIP;
	     _serverPort = serverPort;
	     _clientsPort = clientsPort;
	 }

	public String get_nickname() {
		return _nickname;
	}

	public String get_serverIP() {
		return _serverIP;
	}

	public String get_serverPort() {
		return _serverPort;
	}

	public String get_clientsPort() {
		return _clientsPort;
	}
}
