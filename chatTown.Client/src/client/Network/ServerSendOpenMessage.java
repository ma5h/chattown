package client.Network;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import javax.swing.JOptionPane;

import chatown.OpenMsg;

public class ServerSendOpenMessage  implements Runnable {

	private Socket _socket;
	private ObjectOutputStream _out;
 	private OpenMsg _om;
 	private String _serverIPAddress;
 	private int _serverPort;
 
	
	public ServerSendOpenMessage(OpenMsg om, String IPAddress, int port) {
		_om = om;
		_serverIPAddress = IPAddress;
		_serverPort = port;
	}
	
	@Override
	public void run() {
		
		try {
			_socket = new Socket(_serverIPAddress, _serverPort);
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Error in connection to server");
			e.printStackTrace();
		} 
		    		        
		try { 
			_out = new ObjectOutputStream(_socket.getOutputStream());
			_out.flush();	    
			_out.writeObject(_om);
		} catch (IOException e) {
			e.printStackTrace();
			_socket = null;
		}	

		try {
			_out.close();
			_socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
