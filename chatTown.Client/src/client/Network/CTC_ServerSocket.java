package client.Network;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JOptionPane;

import CustomEvents.CustomEventSource;
import CustomEvents.ICustomEventListener;

import chatown.Client;


public class CTC_ServerSocket implements Runnable {

	private CustomEventSource _eventMsgReceived = new CustomEventSource();
	private CustomEventSource _eventClientDisconnect = new CustomEventSource();
	private ServerSocket _serversocket;
	private Socket _socket;
	private Map<String, CTC_ClientSocket> _clientsMap = Collections.synchronizedMap(new HashMap<String, CTC_ClientSocket>()); // key - client ID
	private int _clientsPort;
	
	public void registerMsgReceivedEvent(ICustomEventListener listener) {
		_eventMsgReceived.addEventListener(listener);
	}
	public void registerClientDisconnectEvent(ICustomEventListener listener) {
		_eventClientDisconnect.addEventListener(listener);
	}
	
	public void connect(int clientsPort) {
		
		_clientsPort = clientsPort;
		System.out.println("clients port: " + _clientsPort);
		if(_serversocket != null)
		{				
			try {
				_serversocket.close();
			} catch (IOException e) {				
				e.printStackTrace();
			}
			_serversocket = null;
		}
		
		try {
			_serversocket = new ServerSocket(_clientsPort);			
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Error in server connection");
			e.printStackTrace();
		}
	}	
	
	public void connectToClient(Client clt) {
		
		if (_clientsMap.containsKey(clt.get_ID())) { return; }
		try {
			Socket socket = new Socket(clt.get_IP(), _clientsPort);
			CTC_ClientSocket clientSocket = new CTC_ClientSocket(socket, _clientsMap, _eventMsgReceived, _eventClientDisconnect);
        	Thread t = new Thread(clientSocket);
        	t.start();	
        	
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Error in connection to client " + clt.get_nickName());
			e.printStackTrace();
		}
	}
	
	public void sendMsgToClient(String clientID, String msg) {
		if (_clientsMap.containsKey(clientID)) {
			_clientsMap.get(clientID).sendMsg(msg);
		}
	}
	
	@Override
	public void run() {
	    while (true) {
        	if (_serversocket == null) {
        		try {
					Thread.sleep(1000);
				} catch (InterruptedException e) { e.printStackTrace(); }
        		
        	} else {
	        		
	        	try {	        	
					_socket = _serversocket.accept();
					CTC_ClientSocket clientSocket = new CTC_ClientSocket(_socket, _clientsMap, _eventMsgReceived, _eventClientDisconnect);
		        	Thread t = new Thread(clientSocket);
		        	t.start();	        	
			        
				} catch (IOException e) {
					_clientsMap.clear();
					e.printStackTrace();
				}
	        }
        }
	}

}
