package client.Network;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;

import chatown.Client;
import chatown.DisconnectMsg;

public class SendDisconnectMessage implements Runnable {

	private Socket _socket;
	private ObjectOutputStream _out;
 	private String _serverIPAddress;
 	private int _serverPort;
 	private Client _client;
 
	
	public SendDisconnectMessage(String IPAddress, int port, Client clt) {
		_serverIPAddress = IPAddress;
		_serverPort = port;
		_client = clt;
	}
	
	@Override
	public void run() {
		
		try {
			_socket = new Socket(_serverIPAddress, _serverPort);
		} catch (IOException e) {
			try {
				_socket.close();
				return;
			} catch (IOException e1) {}
			//e.printStackTrace();
		} 
		    		        
		try { 
			_out = new ObjectOutputStream(_socket.getOutputStream());	    
			_out.writeObject(new DisconnectMsg(_client));
			_out.flush();
		} catch (IOException e) {
			try {
				_out.close();
				_socket.close();
				return;
			} catch (IOException e1) {}
			
			//e.printStackTrace();
			_socket = null;
		}
		
		try {
			_out.close();
			_socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
