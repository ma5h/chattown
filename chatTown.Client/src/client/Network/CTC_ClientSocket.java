package client.Network;


import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.*;

import javax.swing.JOptionPane;

import client.MyClientID;
import client.CustomEvents.ClientFromClientDisconnect;
import client.CustomEvents.MsgReceivedEvent;

import CustomEvents.CustomEventSource;


public class CTC_ClientSocket implements Runnable {
	
	private CustomEventSource _eventMsgReceived;
	private CustomEventSource _eventClientDisconnect;
	private Socket _socket;
	private Map<String, CTC_ClientSocket> _clientsMap; // key - client ID
	private List<String> _msgQueue = Collections.synchronizedList(new LinkedList<String>());
	private String _clientID;
	private ObjectOutputStream _outputStream;
	private ObjectInputStream  _inputstream;


	public CTC_ClientSocket(Socket socket, Map<String, CTC_ClientSocket> clientsMap, 
			CustomEventSource eventMsgReceived, CustomEventSource eventClientDisconnect) {
		_socket = socket;
		_clientsMap = clientsMap;
		_eventMsgReceived = eventMsgReceived;
		_eventClientDisconnect = eventClientDisconnect;
	}

	@Override
	public void run() {
		
		// send my clientID
		try {
			_outputStream = new ObjectOutputStream(_socket.getOutputStream());
			_outputStream.writeObject(MyClientID.get_ID());			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// read clientID
		Object clientObj = null;		
		try {
			_inputstream = new ObjectInputStream(_socket.getInputStream());		
			clientObj  = _inputstream.readObject();					
		} catch (ClassNotFoundException e) { 
			e.printStackTrace(); 
			return; 
		} catch (IOException e) { 
			e.printStackTrace(); 
			return; 
		}	
				
		if (clientObj instanceof String) {
			_clientID = (String)clientObj;	
			_clientsMap.put(_clientID, this);
		}
		
		try {
			_socket.setSoTimeout(200);
		} catch (SocketException e) {			
			e.printStackTrace();
		}
		
		String clientMsg;
		while (_socket != null) {

			// read from client
			try {
				clientObj  = _inputstream.readObject();
			} catch (SocketTimeoutException e) { 
				clientObj = null; 
			} catch (IOException e) {
				try {
					_inputstream.close();
					_outputStream.close();
					_socket.close();
				} catch (IOException e1) {
				} finally {
					_socket = null;
				}
				System.out.println("client from client disconnect 1");
				e.printStackTrace();
			} catch (ClassNotFoundException e) { 
				e.printStackTrace(); 
				continue; 
			}
						
			if ((clientObj != null) && (clientObj instanceof String)) {
				clientMsg = (String)clientObj;
				// send message to UI
				_eventMsgReceived.fireEvent(new MsgReceivedEvent(this, clientMsg, _clientID));	
				clientObj = null;
			} 
			
			// write to client			
			while (!_msgQueue.isEmpty()) {	
				try {		
					_outputStream.writeObject(_msgQueue.get(0));
					_outputStream.flush();
					_msgQueue.remove(0);
				} catch (IOException e) {
					try {
						_inputstream.close();
						_outputStream.close();
						_socket.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					} finally {
						_socket = null;
					}
					e.printStackTrace();
				}				
			}
		}
		
		_eventClientDisconnect.fireEvent(new ClientFromClientDisconnect(this, _clientID));
		// TODO - delete
		System.out.println("client from client disconnect 2");
	}

	public void sendMsg(String msg) {
		_msgQueue.add(msg);
	}
}
