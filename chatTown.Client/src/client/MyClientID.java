package client;

public class MyClientID {
	
	private static String _id = null;
	
	public static void set_ID (String id) {
		if (_id == null) {
			_id = id;
		}
	}
	
	public static String get_ID () {
		return _id;
	}
		
}
