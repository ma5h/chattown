package client;

import java.util.EventObject;

import javax.swing.JFrame;

import CustomEvents.DisconnectClientEvent;
import CustomEvents.ICustomEventListener;
import CustomEvents.OpenMsgClientEvent;
import CustomEvents.SeverConnectionEvent;

import chatown.Globals;
import client.CustomEvents.ClientFromClientDisconnect;
import client.CustomEvents.ConnectToClientEvent;
import client.CustomEvents.MsgReceivedEvent;
import client.CustomEvents.NewClientsListEvent;
import client.CustomEvents.SendMsgToClientEvent;
import client.CustomEvents.SettingsFilledEvent;
import client.Network.CTC_ServerSocket;
import client.Network.SendDisconnectMessage;
import client.Network.ServerConnection;
import client.Network.ServerSendOpenMessage;

public class ClientMain implements ICustomEventListener {

	private static ClientFrame _frame = null;
	private ServerConnection _serverConnection;
	private CTC_ServerSocket _clientsConnection;
	private Thread _networkThread;
	private Thread _clientsThread;
	private String _serverIP;
	private int _serverPort;
	private int _clientsPort;
	
	public static void main(String[] args) {
		Globals.init();
		java.awt.EventQueue.invokeLater(new Runnable() {
        	public void run() {
        		
                _frame = new ClientFrame();
                _frame.setTitle("chatTown");
                _frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                _frame.pack();
                _frame.setVisible(true); 
                
                new ClientMain();
                _frame.openSettingsDialog(true);
        	}
        });

	}
	
	public ClientMain () {
		
		_frame.registerSettingsFilledEvent(this);
		_frame.registerSendOpenMsgEvent(this);
		_frame.registerDisconnectEvent(this);
		
		_frame.registerConnectToClientEvent(this);
		_frame.registerSendMsgToClientEvent(this);
		
		// connection to server
		_serverConnection = new ServerConnection();
		_serverConnection.registerSettingsFilledEvent(this);
		_serverConnection.registerReceiveOpenMsgEvent(this);
		_serverConnection.registerServerConnectionEvent(this);
		
    	_networkThread = new Thread(_serverConnection);    	    
    	_networkThread.start();  
    	
    	// connection to other clients
    	_clientsConnection = new CTC_ServerSocket();
    	_clientsConnection.registerMsgReceivedEvent(this);
    	_clientsConnection.registerClientDisconnectEvent(this);
    	_clientsThread = new Thread(_clientsConnection);    	    
    	_clientsThread.start();  
    	
	}

	@Override
	public void handleEvent(final EventObject e) {

		if (e instanceof SettingsFilledEvent) {
			SettingsFilledEvent ent = (SettingsFilledEvent)e;	
			_serverIP = ent.get_serverIP();
			_serverPort = Integer.parseInt(ent.get_serverPort());
			_clientsPort = Integer.parseInt(ent.get_clientsPort());
			_serverConnection.connect(_serverIP, _serverPort, ent.get_nickname());
			_clientsConnection.connect(_clientsPort);
		} 
		
		else if (e instanceof NewClientsListEvent) {
			
			java.awt.EventQueue.invokeLater(new Runnable() {
	        	public void run() {
	        		NewClientsListEvent ee = (NewClientsListEvent)e;
	        		_frame.setClientslist(ee.get_clientsMap(), ee.get_myClientID());
	        	}
	        });
				
		} 
		
		else if (e instanceof DisconnectClientEvent) {
			SendDisconnectMessage dm = new SendDisconnectMessage(_serverIP, _serverPort, ((DisconnectClientEvent)e).get_client());				
			Thread _sendDisconnectMsgThread = new Thread(dm);
			_sendDisconnectMsgThread.start();
			return;
		}
		
		else if (e instanceof OpenMsgClientEvent) {
			
			if (((OpenMsgClientEvent)e).get_msg().get_client().get_IP() == null) {  // send message to server
				ServerSendOpenMessage sr = new ServerSendOpenMessage(((OpenMsgClientEvent)e).get_msg(), _serverIP, _serverPort);				
				Thread _sendOpenMsgThread = new Thread(sr);
				_sendOpenMsgThread.start();
				return;
			}			
			
			java.awt.EventQueue.invokeLater(new Runnable() { // handle OpenMsg from server
	        	public void run() {	        		
	        		_frame.handleOpenMsg(((OpenMsgClientEvent)e).get_msg());
	        	}
	        });
			
		}
		
		else if (e instanceof ConnectToClientEvent) {
			_clientsConnection.connectToClient(((ConnectToClientEvent)e).get_client());
		}
		
		else if (e instanceof SendMsgToClientEvent) {
			_clientsConnection.sendMsgToClient(((SendMsgToClientEvent)e).get_clientID(), ((SendMsgToClientEvent)e).get_msg());
		}
		
		else if (e instanceof MsgReceivedEvent) {
			java.awt.EventQueue.invokeLater(new Runnable() { // handle OpenMsg from server
	        	public void run() {	        		
	        		_frame.handleClientMsg(((MsgReceivedEvent)e).get_msg(), ((MsgReceivedEvent)e).get_senderID());
	        	}
	        });	
		}
		
		else if (e instanceof SeverConnectionEvent) {
			java.awt.EventQueue.invokeLater(new Runnable() { // handle OpenMsg from server
	        	public void run() {	        		
	        		_frame.handleServerConnection(((SeverConnectionEvent)e).isServerConnected());
	        	}
	        });	
		}
		
		else if (e instanceof ClientFromClientDisconnect) {
			java.awt.EventQueue.invokeLater(new Runnable() { // handle OpenMsg from server
	        	public void run() {	        		
	        		_frame.clientDisconnect(((ClientFromClientDisconnect)e).get_clientID());
	        	}
	        });
		}
		
	}
	
}
