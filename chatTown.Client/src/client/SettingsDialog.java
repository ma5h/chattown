package client;

import java.awt.BorderLayout;
import java.awt.Image;

import javax.swing.InputVerifier;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;

import chatown.Globals;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.Component;
import org.eclipse.wb.swing.FocusTraversalOnArray;

public class SettingsDialog extends JDialog {

	private static final long serialVersionUID = -6527767413859354483L;
	private final JPanel contentPanel = new JPanel();
	public boolean OK = false;
	public boolean CANCEL = false;
	private JButton okButton;
	private JButton cancelButton;
	private JTextField tfNickname;
	private InputVerifier _nicknameVerifier;
	private JLabel lblServerPort;
	private JLabel lblClientsPort;
	private JLabel lblServerIP;
	private boolean _isDisableCanecl = false;

	public SettingsDialog() {
		loadIcon();
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowOpened(WindowEvent e) {
				tfNickname.requestFocusInWindow();
			}
		});
		
		setTitle("Settings");
		setModal(true);
		init_look();
		
		setBounds(100, 100, 500, 261);		
		
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		JLabel lbl4 = new JLabel("Server IP : ");		
		JLabel lbl1 = new JLabel("Server port : ");
		JLabel lbl7 = new JLabel("Clients port : ");
		{
			okButton = new JButton("OK");
			okButton.setEnabled(false);
			okButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (!_nicknameVerifier.verify(tfNickname))
					{
						tfNickname.requestFocusInWindow();
						return;
					}
					OK = true;
					setVisible(false);
				}
			});
			okButton.setActionCommand("OK");
			getRootPane().setDefaultButton(okButton);
		}
		{
			cancelButton = new JButton("Cancel");
			cancelButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					if (!_nicknameVerifier.verify(tfNickname))
					{
						tfNickname.requestFocusInWindow();
						return;
					}
					CANCEL = true;
					setVisible(false);
				}
			});
			cancelButton.setActionCommand("Cancel");
		}
		
		JLabel lblNickname = new JLabel("nickname : ");
		
		tfNickname = new JTextField();	
		
		// Listen for changes in the text
		tfNickname.getDocument().addDocumentListener(new DocumentListener() {
			public void changedUpdate(DocumentEvent e) {
			// text was changed
				if (_nicknameVerifier.verify(tfNickname)) {
					okButton.setEnabled(true);
				}
			}
			public void removeUpdate(DocumentEvent e) {
			// text was deleted
				if (_nicknameVerifier.verify(tfNickname)) {
					okButton.setEnabled(true);
				}
			}
			public void insertUpdate(DocumentEvent e) {
			// text was inserted
				if (_nicknameVerifier.verify(tfNickname)) {
					okButton.setEnabled(true);
				}
			}
		});
		
		tfNickname.setColumns(10);
		
		lblServerIP = new JLabel("");		
		lblServerPort = new JLabel("");		
		lblClientsPort = new JLabel("");
		
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel.setHorizontalGroup(
			gl_contentPanel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addGap(20)
					.addComponent(lblNickname)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(tfNickname, GroupLayout.PREFERRED_SIZE, 123, GroupLayout.PREFERRED_SIZE)
					.addGap(83)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(lbl4)
						.addComponent(lbl1)
						.addComponent(lbl7))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addComponent(lblClientsPort)
						.addComponent(lblServerIP)
						.addComponent(lblServerPort))
					.addGap(61, 61, Short.MAX_VALUE))
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addContainerGap(243, Short.MAX_VALUE)
					.addComponent(okButton)
					.addGap(18)
					.addComponent(cancelButton)
					.addGap(101))
		);
		gl_contentPanel.setVerticalGroup(
			gl_contentPanel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPanel.createSequentialGroup()
					.addGap(28)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
							.addComponent(tfNickname, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addComponent(lblNickname))
						.addGroup(gl_contentPanel.createSequentialGroup()
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
								.addComponent(lbl4)
								.addComponent(lblServerIP))
							.addGap(18)
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
								.addComponent(lbl1)
								.addComponent(lblServerPort))
							.addGap(27)
							.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
								.addComponent(lbl7)
								.addComponent(lblClientsPort))))
					.addPreferredGap(ComponentPlacement.RELATED, 64, Short.MAX_VALUE)
					.addGroup(gl_contentPanel.createParallelGroup(Alignment.BASELINE)
						.addComponent(cancelButton)
						.addComponent(okButton))
					.addContainerGap())
		);
		contentPanel.setLayout(gl_contentPanel);
						
		//------------------------
		
		lblClientsPort.setText(Integer.toString(Globals.DEFAULT_CLIENT_PORT));
		lblServerPort.setText(Integer.toString(Globals.DEFAULT_SERVER_PORT));
		lblServerIP.setText(Globals.DEFAULT_SERVER_IP);		
		contentPanel.setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{lblNickname, tfNickname, lbl4, lblServerIP, lbl1, lblServerPort, lbl7, lblClientsPort, okButton, cancelButton}));
		
		_nicknameVerifier = new InputVerifier() {
	    	public boolean verify(JComponent comp) {
	          
	    		JTextField textField = (JTextField) comp;
	    		return (!textField.getText().isEmpty());
	    	}
	    };

	    //tfNickname.setInputVerifier(_nicknameVerifier);
	}	
	
	private void loadIcon() {
		Image img = getToolkit().getImage(getClass().getResource("/resources/chat.png"));
		this.setIconImage(img);
	}
	
	public String getServerPort() {
		return lblServerPort.getText();
	}
	public String getServerIP() {
		return lblServerIP.getText();
	}
	public String getClientsPort() {
		return lblClientsPort.getText();
	}
	
	public String getNickname() {
		return tfNickname.getText();
	}  
	
	public void disableNickname() {
		tfNickname.setEditable(false);
		tfNickname.setFocusable(false);
	} 
	
	public void setNickname(String _nickname) {
		tfNickname.setText(_nickname);		
	}
	
	public boolean isResizable() {
	    return true;
	}

//	private Boolean validateIP(String ipAddress) {		
//
//		final String PATTERN = 
//		        "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
//		        "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
//		        "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
//		        "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
//
//		Pattern pattern = Pattern.compile(PATTERN);
//		Matcher matcher = pattern.matcher(ipAddress);
//		return matcher.matches();   
//	}
	
	private void init_look() {

//        try {
//        	UIManager.setLookAndFeel(new NimbusLookAndFeel() {			
//
//				private static final long serialVersionUID = 8658247572342697905L;
//
//				@Override
//			    public UIDefaults getDefaults() {
//				     UIDefaults ret = super.getDefaults();
//				     ret.put("defaultFont", new Font(Font.SANS_SERIF, Font.PLAIN, 13));
//				     return ret;
//			    }
//			});
//        //} catch (UnsupportedLookAndFeelException e) {
//        } catch (Exception e) {
//			e.printStackTrace();
//		}
    }
	
	protected void processWindowEvent(WindowEvent e) {

		if (e.getID() == WindowEvent.WINDOW_CLOSING) {
			
			if (_isDisableCanecl) {
				return;
			}
			
			if (!_nicknameVerifier.verify(tfNickname))
			{
				tfNickname.requestFocusInWindow();
				return;				
			}
        }
		
		super.processWindowEvent(e);
	}

	public void disableCancel() {
		_isDisableCanecl = true;
		cancelButton.setEnabled(false);		
	}
}
